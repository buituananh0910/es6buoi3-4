import { congViec1 } from "../model/task.model.js";

export function layThongTinTuForm() {
  let task = document.getElementById("newTask").value;

  let congViec = new congViec1(task);
  return congViec;
}

export function showCongViecLenForm(congViec) {
  document.getElementById("newTask").value = congViec.task;
}

export function renderDanhSachCongViec(congViec) {
  let contentHTML = " ";
  congViec.forEach((item) => {
    let contentTr = `
      <li>
        ${item.task}
        <a> 
        <button onclick="xoaCongViec('${item.task}')" class="btn btn-warning">
        <i class="fa-solid fa-trash-can"></i>
        </button>
        <button>
            <input id="item" type="checkbox">
        </button>
        </a>
       
        
      </li>
      `;
    contentHTML += contentTr;
  });
  document.getElementById("todo").innerHTML = contentHTML;
}
