import {
  layThongTinTuForm,
  renderDanhSachCongViec,
} from "./controller/task.controller.js";

let danhSachCongViec = [];

document.getElementById("addItem").addEventListener("click", function () {
  let congViec = layThongTinTuForm();

  danhSachCongViec.push(congViec);
  renderDanhSachCongViec(danhSachCongViec);
});

function xoaCongViec(id) {
  let index = danhSachCongViec.findIndex((item) => {
    return item.task == id;
  });
  if (index !== -1) {
    danhSachCongViec.splice(index, 1);
    renderDanhSachCongViec(danhSachCongViec);
  }
}
window.xoaCongViec = xoaCongViec;
